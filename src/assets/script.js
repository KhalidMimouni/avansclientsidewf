

function displaybuttons(){
    document.getElementById("cancelButton").style.visibility = "visible"
    document.getElementById("saveButton").style.visibility = "visible"
}

function removeButtons(){
    document.getElementById("cancelButton").style.visibility = "hidden"
    document.getElementById("saveButton").style.visibility = "hidden"
}

function makeInputFieldsEditable(){
    document.getElementById("IdLabel").removeAttribute("readonly");
    document.getElementById("firstNameLabel").removeAttribute("readonly");
    document.getElementById("secondNameLabel").removeAttribute("readonly");
    document.getElementById("emailLabel").removeAttribute("readonly");
    document.getElementById("phoneNumberLabel").removeAttribute("readonly");
    document.getElementById("genders").removeAttribute("disabled")
}

function closeInputFields(){
    document.getElementById("IdLabel").setAttribute("readonly", true);
    document.getElementById("firstNameLabel").setAttribute("readonly", true);
    document.getElementById("secondNameLabel").setAttribute("readonly", true);
    document.getElementById("emailLabel").setAttribute("readonly", true);
    document.getElementById("phoneNumberLabel").setAttribute("readonly", true);
    document.getElementById("genders").setAttribute("disabled", true)
}

function updateUser(){
    var user = {
    id: parseInt(document.getElementById("IdLabel").value, 10),
    firstName: document.getElementById("firstNameLabel").value,
    lastName: document.getElementById("secondNameLabel").value,
    phoneNumber: document.getElementById("phoneNumberLabel").value,
    email: document.getElementById("emailLabel").value,
    gender: document.getElementById("genders").value
    }
    console.log("de nieuwe user is :" + JSON.stringify(user))
    return user;
    
}
