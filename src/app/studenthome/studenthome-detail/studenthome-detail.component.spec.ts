import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudenthomeDetailComponent } from './studenthome-detail.component';

describe('StudenthomeDetailComponent', () => {
  let component: StudenthomeDetailComponent;
  let fixture: ComponentFixture<StudenthomeDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudenthomeDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudenthomeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
