import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Address } from '../Address';
import { IStudentHomeRepo } from '../IStudentHomeRepo';
import { StudentHome } from '../StudentHome';
import { StudentHomeDatasharing } from '../StudentHomeDataSharing';

@Component({
  selector: 'app-studenthome-detail',
  templateUrl: './studenthome-detail.component.html',
  
  
})
export class StudenthomeDetailComponent implements OnInit, OnDestroy {
  studentHome!: StudentHome
  model!: StudentHome;
  subscription!: Subscription
  public isCancelAndSaveButtonVisible: boolean = false
  public readonlyValue: string | null = '';
  

  constructor(private dataSharingService: StudentHomeDatasharing, private studentHomeRepo: IStudentHomeRepo) { }

  ngOnInit(): void {
    this.dataSharingService.currentStudentHome.subscribe(studenthome => {this.studentHome = studenthome, this.model = new StudentHome(studenthome)});
    console.log(this.model._id)
  }
  ngOnDestroy(): void{
    if(this.subscription){
      this.subscription.unsubscribe();
    }
    
  }

  deleteStudentHome(studentHome: StudentHome): void{
    this.studentHomeRepo.deleteStudentHomeApi(studentHome);
  
  }

  setCancelAndSaveVisibility(isVisible: boolean){
    this.isCancelAndSaveButtonVisible = isVisible;
  }
  setReadonly(setAttribute: boolean){
    if(setAttribute){
      this.readonlyValue = ''
    }else{
      this.readonlyValue = null
    }
  }
  updateStudentHome(): void{
    this.model.updatedOn = new Date();
    this.studentHomeRepo.updateStudentHomeApi(this.model);
  }

  resetFormModel(): void{
    this.model = new StudentHome(this.studentHome);
  }

}
