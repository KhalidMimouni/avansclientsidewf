import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable} from "rxjs";
import { StudentHomeRepoService } from "../service/student-home-repo.service";
import { StudentHome } from "./StudentHome";
@Injectable({
    providedIn: 'root'
})
export class StudentHomeDatasharing{
    public dataSource:BehaviorSubject<StudentHome>;
    public currentStudentHome!: Observable<StudentHome>;
    constructor(private studentHomeRepo: StudentHomeRepoService){
        this.dataSource = new BehaviorSubject<StudentHome>(studentHomeRepo.getStudentHomes()[0])
        this.currentStudentHome = this.dataSource.asObservable();
    }
    changeCurrentStudentHome(studentHome: StudentHome){
        console.log(JSON.stringify(studentHome));
        this.dataSource.next(studentHome);
    }
}