import { Address } from "./Address";


export class StudentHome{
    _id?: string;
    name?: string;
    adminId?: number;
    address?: Address;
    phoneNumber?: string;
    createdOn?: Date;
    updatedOn?: Date;

    constructor(idOrStudentHome?: string | StudentHome, name?: string, adminId?: number, address?: Address, phonenumber?: string,  studentHome?: StudentHome){
        if(typeof idOrStudentHome === 'object'){
            console.log("diepe constructor aangeroepen")
            this._id = idOrStudentHome._id;
            this.name = idOrStudentHome.name;
            this.adminId = idOrStudentHome.adminId;
            this.address = new Address(idOrStudentHome.address)
            this.phoneNumber = idOrStudentHome.phoneNumber;
            this.createdOn = new Date(idOrStudentHome.createdOn!)
            this.updatedOn = idOrStudentHome.updatedOn ? new Date(idOrStudentHome.updatedOn) : undefined
            
            
        }else{
            console.log("shallow constructor aangeroepen")
            this._id = idOrStudentHome;
            this.name = name;
            this.adminId = adminId;
            if(address){
                this.address = address;
            }else{
                this.address = new Address();
            }
            this.phoneNumber = phonenumber;
        }
        
        
        
    }
}