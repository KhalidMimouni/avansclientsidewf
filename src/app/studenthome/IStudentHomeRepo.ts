import { Observable } from "rxjs";
import { StudentHome } from "./StudentHome";

export abstract class IStudentHomeRepo{
    public abstract getStudentHomes(): StudentHome[];
    public abstract addStudentHome(studentHome: StudentHome): void;
    public abstract getStudentHomeById(id: string): StudentHome;
    public abstract deleteStudentHome(studentHome: StudentHome): void;
    public abstract updateStudentHome(studentHomeWithNewValues: StudentHome): void;
    public abstract getStudentHomesApi(options?: any): Observable<StudentHome[]>;
    public abstract addStudentHomeApi(studentHome: StudentHome, options?: any): void;
    public abstract updateStudentHomeApi(studenthome: StudentHome, options?: any): void;
    public abstract deleteStudentHomeApi(studenthome: StudentHome, options?: any): void;

}