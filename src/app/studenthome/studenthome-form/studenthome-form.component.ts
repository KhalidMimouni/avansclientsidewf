import { Component, OnInit } from '@angular/core';
import { Address } from '../Address';
import { IStudentHomeRepo } from '../IStudentHomeRepo';
import { StudentHome } from '../StudentHome';

@Component({
  selector: 'app-studenthome-form',
  templateUrl: './studenthome-form.component.html',
  styleUrls: ['../../user/user-form/forms.css']
})
export class StudenthomeFormComponent implements OnInit {
  model: StudentHome = new StudentHome();
  
  constructor(private studentHomeRepo: IStudentHomeRepo) {

   }

  ngOnInit(): void {
  }

  public addStudentHome(studentHome: StudentHome){
    var newStudentHome: StudentHome = new StudentHome(studentHome._id, studentHome.name, studentHome.adminId, 
      new Address(studentHome.address!.street, studentHome.address!.number, studentHome.address!.city, studentHome.address!.zipcode), studentHome.phoneNumber);
    newStudentHome.createdOn = new Date();
    console.log("nieuwe studenthome aangemaakt")
    this.studentHomeRepo.addStudentHomeApi(newStudentHome);
  }

}
