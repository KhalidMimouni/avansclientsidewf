import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudenthomeFormComponent } from './studenthome-form.component';

describe('StudenthomeFormComponent', () => {
  let component: StudenthomeFormComponent;
  let fixture: ComponentFixture<StudenthomeFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudenthomeFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudenthomeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
