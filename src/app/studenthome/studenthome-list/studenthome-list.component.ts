import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs';
import { Address } from '../Address';
import { IStudentHomeRepo } from '../IStudentHomeRepo';
import { StudentHome } from '../StudentHome';
import { StudentHomeDatasharing } from '../StudentHomeDataSharing';


@Component({
  selector: 'app-studenthome-list',
  templateUrl: './studenthome-list.component.html',
  styles: [
  ]
})
export class StudenthomeListComponent implements OnInit {
  studentHomeList!: StudentHome[];
  
  constructor(private studentHomeRepo: IStudentHomeRepo, private dataSharingService: StudentHomeDatasharing) { 

  }

  ngOnInit(): void {
    
    this.studentHomeRepo.getStudentHomesApi()
    .subscribe(studenthomes =>  studenthomes.map(studenthome => 
      (studenthome.createdOn = new Date(studenthome.createdOn!),
      studenthome.updatedOn = studenthome.updatedOn? new Date(studenthome.updatedOn) : undefined), 
      this.studentHomeList = studenthomes ))
    
    
  }

  onClickStudentHome(studenthome: StudentHome): void{
    console.log(this.studentHomeList[0].address?.city)
    this.dataSharingService.changeCurrentStudentHome(studenthome);
  }

}
