import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StudentHome } from '../StudentHome';
import { Address } from '../Address';
import { StudenthomeListComponent } from './studenthome-list.component';
import { IStudentHomeRepo } from '../IStudentHomeRepo';

describe('StudenthomeListComponent', () => {
  let component: StudenthomeListComponent;
  let fixture: ComponentFixture<StudenthomeListComponent>;
  let studentHomeRepoSpy;
  let studentHomeDataSharingSpy;
  var avans = new StudentHome('1', 'Huize Avans', 1, new Address('Lovensdijkstraat', 63), '0645263188');
  avans.createdOn = new Date();
  var fontys = new StudentHome('2', 'Huize Fontys', 1, new Address('Fontysstraat', 200), '0645772388');
  fontys.createdOn = new Date();
  var studententent = new StudentHome('3', 'Studenten Tent', 1, new Address('Tentstudentstraat', 55), '0642455188');
  studententent.createdOn = new Date();

  const studentHomes: StudentHome[] = [avans, fontys, studententent]
  

  

  beforeEach(async () => {
    studentHomeRepoSpy = jasmine.createSpyObj('StudentHomeRepoService', ['getStudentHomes'])
    studentHomeRepoSpy.getStudentHomes.and.returnValue(studentHomes)
    
    await TestBed.configureTestingModule({
      declarations: [ StudenthomeListComponent ],
      providers:[
        {provide: IStudentHomeRepo, useValue: studentHomeRepoSpy},
        ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudenthomeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('Should contain a list of studenthomes', (done: DoneFn) => {
    fixture.detectChanges();
    expect(component.studentHomeList.length).toBe(3);
    done();
  });
  

});
