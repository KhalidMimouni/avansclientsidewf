export class Address{
    street?: string;
    number?: number;
    city?: string;
    zipcode?: string;

    constructor(streetNameOrAddress?: string | Address, number?: number, city?: string, zipcode?: string){
        if(typeof streetNameOrAddress === 'object'){
            this.street = streetNameOrAddress.street;
            this.number = streetNameOrAddress.number;
            this.city = streetNameOrAddress.city;
            this.zipcode = streetNameOrAddress.zipcode;
        }else{
            this.street = streetNameOrAddress;
            this.number = number;
            this.city = city;
            this.zipcode = zipcode;
        }
        
    }
}