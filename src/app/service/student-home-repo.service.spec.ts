import { TestBed } from '@angular/core/testing';

import { StudentHomeRepoService } from './student-home-repo.service';

describe('StudentHomeRepoService', () => {
  let service: StudentHomeRepoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StudentHomeRepoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should return a list of studenthomes', (done: DoneFn) => {
    var listOfStudentHomes = service.getStudentHomes();
    expect(listOfStudentHomes.length).toBe(6);
    done()
  });
  it('should return a studenthome by given Id', (done: DoneFn) => {
    var studentHome = service.getStudentHomeById(1);
    expect(studentHome.id).toBe(1);
    done()
  });
});
