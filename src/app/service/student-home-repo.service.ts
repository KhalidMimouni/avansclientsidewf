import { Injectable } from "@angular/core";
import { IStudentHomeRepo } from "../studenthome/IStudentHomeRepo"; 
import { StudentHome } from "../studenthome/StudentHome";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { catchError, Observable, tap, throwError } from "rxjs";
import { environment } from "src/environments/environment";
@Injectable({providedIn: 'root'})
export class StudentHomeRepoService implements IStudentHomeRepo{
    studentHomeList: StudentHome[];
    readonly BASE_URL: string = environment.apiUrl + "studenthomes";
    readonly httpOptions = {
        observe: "body",
        responseType: "json"
    }

    constructor(private httpClient: HttpClient){
        this.studentHomeList = [];
        

    }
    private handleError(error: HttpErrorResponse): Observable<any> {
        console.log(error);
    
        return throwError(() => error);
      }
    public getStudentHomes(): StudentHome[] {
        return this.studentHomeList;
    }
    public addStudentHome(studentHome: StudentHome): void {
        this.studentHomeList.push(studentHome);
    }
    public getStudentHomeById(id: string): StudentHome {
        return this.studentHomeList.find(s => s._id == id)!;
    }
    public deleteStudentHome(studentHome: StudentHome): void {
        this.studentHomeList.splice(this.studentHomeList.indexOf(studentHome),1);
    }

    public updateStudentHome(studenthome: StudentHome): void {
        var indexOfOldStudentHome = this.studentHomeList.findIndex(s => s._id == studenthome._id)
        studenthome.updatedOn = new Date();
        this.studentHomeList[indexOfOldStudentHome] = new StudentHome(studenthome)
    }
    public getStudentHomesApi(options?: any): Observable<StudentHome[]>{
        return this.httpClient.get<StudentHome[]>(this.BASE_URL, {...options, ...this.httpOptions})
        .pipe(tap(console.log), catchError(this.handleError))
    }

    public addStudentHomeApi(studentHome: StudentHome, options?: any){
        console.log("in de studenthome repo service")
        return this.httpClient
        .post<StudentHome>(this.BASE_URL, {...options, studentHome, ...this.httpOptions})
        .pipe(tap(console.log), catchError(this.handleError)).subscribe();
    }

    public updateStudentHomeApi(studentHome: StudentHome, options?: any){
        console.log("updateStudentHome aangeroepen in de studenthomerepo service")
        return this.httpClient
        .put<StudentHome>(this.BASE_URL+`/${studentHome._id}`, {...options, studentHome, ...this.httpOptions})
        .pipe(tap(console.log), catchError(this.handleError)).subscribe();
    }
    public deleteStudentHomeApi(studentHome: StudentHome, options?: any){
        console.log("deleteStudentHome aangeroepen in studenthomerepo service")
        return this.httpClient
        .delete<StudentHome>(this.BASE_URL+`/${studentHome._id}`, {...options, ...this.httpOptions})
        .pipe(tap(console.log),catchError(this.handleError)).subscribe();
    }

}
