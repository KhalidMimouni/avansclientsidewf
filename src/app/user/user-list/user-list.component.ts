import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { User } from '../User';
import { IUserRepo } from '../IUserRepo';
import { DataSharingService } from '../DataSharingService';






@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html'
})
export class UserListComponent implements OnInit {
  userList:User[] = [];
  
  
  
  constructor(private userRepo: IUserRepo, private dataSharingService: DataSharingService) { 
  }

  ngOnInit(): void {
    this.userList = this.userRepo.getUsers();
  
  }

  onClickUser(newUser: User){
    // console.log("De ID van de user is: " + newUser.id);
    // this.userRepo.addUser(new User(3, 'Ruud', 'Hermans', 6393043, 'rh@hotmail.com'))
    this.dataSharingService.changeCurrentUser(newUser);
    console.log("De huidige lijst in de UserListComponent is: " +  JSON.stringify(this.userList));
  }

  

}
