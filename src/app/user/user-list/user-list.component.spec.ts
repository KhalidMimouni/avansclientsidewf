import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DataSharingService } from '../DataSharingService';
import { IUserRepo } from '../IUserRepo';
import { UserRepo } from '../UserRepo';

import { UserListComponent } from './user-list.component';

describe('UserListComponent', () => {
  let component: UserListComponent;
  let fixture: ComponentFixture<UserListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserListComponent ],
      imports:[],
      providers:[UserRepo, DataSharingService,{provide: IUserRepo, useExisting: UserRepo}]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
