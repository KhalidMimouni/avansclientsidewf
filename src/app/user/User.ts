export enum Gender{
    Male = "Male",
    Female = "Female"
}

export class User{
    id?: number;
    firstName?: string;
    lastName?: string;
    phoneNumber?: string;
    email?: string;
    birthDate?: Date
    gender?: Gender

    constructor(id?: number, firstName?:string, lastName?:string, phoneNumber?:string , email?:string, birthDate?: Date, gender?: Gender){
        this.id = id;
        this.firstName= firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.birthDate = birthDate;
        this.gender = gender;
        
    }

    getBirthDateString(){
        return this.birthDate?.toLocaleString('en-GB', { dateStyle: 'short' })
    }

    
    
}