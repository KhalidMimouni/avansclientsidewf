import { Injectable } from "@angular/core";
import { IUserRepo } from "./IUserRepo";
import { User } from "./User";
import { Gender } from "./User";
@Injectable()
export class UserRepo implements IUserRepo{
    userList:User[];

    constructor(){
        this.userList = [];
        var Khalid = new User(1, 'Khalid', 'Mimouni', "0639308067", 'khalid_mi@hotmail.com', new Date("1995-11-04"), Gender.Male);
        var Hans = new User(2, 'Hans', 'Klok', "0639308067", 'hansklok@hotmail.com', new Date("1990-11-04"), Gender.Male);
        var Dion = new User(3, 'Dion', 'Koeze', "068569874", 'dk@hotmail.com@hotmail.com', new Date("1985-11-04"), Gender.Male);
        var Robin = new User(4, 'Robin', 'Schellius', "0634545067", 'robins@hotmail.com', new Date("1950-11-04"), Gender.Male);
        var Ruud = new User(5, 'Ruud', 'Hermans', "063545067", 'ruudh@hotmail.com', new Date("1960-11-04"), Gender.Male);
        var Arno = new User(6, 'Arno', 'Broeders', "0639341067", 'arnob@hotmail.com', new Date("1976-11-04"), Gender.Male);
        var Davide = new User(7, 'Davide', 'Ambesi', "639308067", 'davidea@hotmail.com', new Date("1978-11-04"), Gender.Male);
        this.userList.push(Khalid, Hans, Dion, Robin, Ruud, Arno, Davide);
    }
    getUsers(): User[] {
        return this.userList;
    }
    getUserById(id: number): User | undefined{
        return this.userList.find(u => u.id == id);
    }

    addUser(user: User){
        this.userList.push(user);
        console.log("user dat toegevoegd wordt in de repo: " + JSON.stringify(user))
        console.log("gender van de persoon dat toegevoegd wordt in de po: " + user.gender)
        console.log("de lijst van users in de repo is nu: " + JSON.stringify(this.userList));
    }

    deleteUser(user: User|undefined){
        if(user instanceof User){
            this.userList.splice(this.userList.indexOf(user),1);
        }
        
    }

}