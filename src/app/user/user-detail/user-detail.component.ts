import { Component, OnDestroy, OnInit } from '@angular/core';
import { DataSharingService } from '../DataSharingService';
import { User } from '../User';
import { IUserRepo } from '../IUserRepo';
import {Gender} from '../User';
import { Subscription } from 'rxjs';

declare function displaybuttons(): void;
declare function makeInputFieldsEditable(): void;
declare function closeInputFields(): void;
declare function removeButtons(): void;
declare function updateUser(): {id: number, firstName: any, lastName: any, phoneNumber: any, email: any, gender: any};
@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit, OnDestroy {
  user!: User
  subscription!: Subscription
  constructor(private dataSharingService: DataSharingService, private userRepo: IUserRepo) { 
    
  }
    
  ngOnInit(): void {
    this.subscription = this.dataSharingService.currentUser.subscribe(user => this.user = user)
  }
  ngOnDestroy(): void{
    if(this.subscription){
      this.subscription.unsubscribe();
    }
    
  }

  deleteUser(user: User|undefined){
    this.userRepo.deleteUser(user);
  }
  displayButtons(){
    displaybuttons();
  }
  openInputFields(){
    makeInputFieldsEditable();
  }

  closeFields(){
    closeInputFields();
  }

  hideButtons(){
    removeButtons();
  }
  updateSelectedUser(){
    var userWithNewValues = updateUser();
    if(this.user instanceof User){
      this.user.id = userWithNewValues.id;
      this.user.firstName = userWithNewValues.firstName;
      this.user.lastName = userWithNewValues.lastName;
      this.user.phoneNumber = userWithNewValues.phoneNumber;
      if(userWithNewValues.gender == "Male"){
        this.user.gender = Gender.Male;
      }else{
        this.user.gender = Gender.Female;
      }
      this.user.email = userWithNewValues.email;
    }
  }
  getBirthDateString(user: User){
    if(this.user instanceof User){
      return this.user.birthDate?.toISOString().substr(0, 10)
    }else{
      return null;
    }
    
}



}
