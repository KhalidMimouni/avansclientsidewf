import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { User } from "./User";
import { IUserRepo } from "./IUserRepo";
@Injectable({
    providedIn: 'root'
})
export class DataSharingService{
    public dataSource:BehaviorSubject<User>;
    public currentUser!: Observable<User>;
    constructor(private userRepo:IUserRepo){
        this.dataSource = new BehaviorSubject<User>(userRepo.getUsers()[0]);
        this.currentUser = this.dataSource.asObservable();
        console.log("DataSharingService geinstantieerd")
    }
    changeCurrentUser(newUser: User){
        this.dataSource.next(newUser)
    }
}