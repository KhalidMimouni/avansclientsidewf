import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { User } from '../User';
import { Gender } from '../User';
import { IUserRepo } from '../IUserRepo';



@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./forms.css']
})
export class UserFormComponent implements OnInit {
  model: User = new User();
  
  
  
  submitted = false;
  
  constructor(private userRepo: IUserRepo) { }

  ngOnInit(): void {
    
  }

  onSubmit(){
    this.submitted = true;
  }

  public addUser(user: User){
    console.log("De aangemaakte user in de user form: " + JSON.stringify(user))
    if(user.birthDate && user.gender){
      var gender;
      if (user.gender == "Male"){
        gender = Gender.Male;
      }else{
        gender = Gender.Female;
      }
      this.userRepo.addUser(new User(user.id, user.firstName, user.lastName, user.phoneNumber, user.email, new Date(user.birthDate), gender))
      
    }
    
    
    
  }

}
