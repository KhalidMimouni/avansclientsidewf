import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { StudentHome } from './studenthome/StudentHome';
import { StudenthomeDetailComponent } from './studenthome/studenthome-detail/studenthome-detail.component';
import { StudenthomeFormComponent } from './studenthome/studenthome-form/studenthome-form.component';
import { StudenthomeComponent } from './studenthome/studenthome.component';
import { UserDetailComponent } from './user/user-detail/user-detail.component';
import { UserFormComponent } from './user/user-form/user-form.component';
import { UserComponent } from './user/user.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'About'},
  {path:'Users', component: UserComponent, children: [
    {path: 'New', pathMatch: 'full', component: UserFormComponent},
    {path: ':id', pathMatch: 'full', component: UserDetailComponent},
  ]},
  {path: 'About', pathMatch: 'full', component: AboutComponent},
  {path: 'StudentHomes', component: StudenthomeComponent, children:[
    {path: 'New', pathMatch: 'full', component: StudenthomeFormComponent},
    {path: ':id', pathMatch: 'full', component: StudenthomeDetailComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
