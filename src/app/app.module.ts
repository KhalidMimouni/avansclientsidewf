import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NavComponent } from './shared/nav/nav.component';
import { AboutComponent } from './about/about.component';
import { UserComponent } from './user/user.component';
import { UserListComponent } from './user/user-list/user-list.component';
import { UserDetailComponent } from './user/user-detail/user-detail.component';
import { IUserRepo } from './user/IUserRepo';
import { UserRepo } from './user/UserRepo';
import { UserFormComponent } from './user/user-form/user-form.component';
import { FormsModule } from '@angular/forms';
import { UsecaseComponent } from './about/usecases/usecase.component';
import { FooterComponent } from './footer/footer.component';
import { StudenthomeComponent } from './studenthome/studenthome.component';
import { StudentHomeRepoService } from './service/student-home-repo.service'; 
import { IStudentHomeRepo } from './studenthome/IStudentHomeRepo';
import { StudenthomeListComponent } from './studenthome/studenthome-list/studenthome-list.component';
import { StudenthomeDetailComponent } from './studenthome/studenthome-detail/studenthome-detail.component';
import { StudenthomeFormComponent } from './studenthome/studenthome-form/studenthome-form.component';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    AboutComponent,
    UserComponent,
    UserListComponent,
    UserDetailComponent,
    UserFormComponent,
    UsecaseComponent,
    FooterComponent,
    StudenthomeComponent,
    StudenthomeListComponent,
    StudenthomeDetailComponent,
    StudenthomeFormComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    NgbModule,
    FormsModule
  ],
  providers: [UserRepo,  StudentHomeRepoService, {provide: IUserRepo, useExisting: UserRepo}, {provide: IStudentHomeRepo, useExisting: StudentHomeRepoService}],
  bootstrap: [AppComponent]
})
export class AppModule { }
